"use strict"
console.log ('1. let name1 = 5 or const name2 = 4, ініціалізацією за допомогою let або const. Є ще застарілий варіант var');
console.log('2. У JavaScript рядок (string) - це послідовність символів, яку можна використовувати для представлення текстової інформації. Можна створити з одинарними, подвійними або зворотніми рядками: ');

let exampleForString1 = 'Example1';
let exampleForString2 = "Example2";
const exampleForString3 = `Example3`;
let exampleNeWString = new String('Example4');
let numberToStringExample = 5;
let strNumberToStringExample= String(numberToStringExample ); console.log('приводить інший тип данних до string');

console.log('3. виконати команду console.log = (typeof <let or const>)');

let checkOfDataType = 5;
console.log(checkOfDataType);
console.log(typeof checkOfDataType);

console.log('4. Через Конкатенацію або Склеювання рядків, одиниця перетворюється на тип string і дві одиниці склеюються');

console.log('Practice Tasks');

console.log('Завдання 1');

let a = 5;
console.log(typeof a);

console.log('Завдання 2');

let nameUser = "Oleksandr", lastNameUser = "Nimets";
let message = `Мене звати ${nameUser} ${lastNameUser}`;
console.log(message);

console.log('Завдання 3');

let b = 'Education';
let message1 = `My ${b} is ongoing`;
console.log(message1);
